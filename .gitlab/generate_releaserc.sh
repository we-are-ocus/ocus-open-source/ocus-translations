#!/bin/bash -e

#Build .releaserc.json  dynamically with all assets in src/ directory

shopt -s globstar && for i in src/**/*.*; do j='{"path": "'$i'", "label": "'$i'"}';jq --argjson values "$j" '.plugins[-1][-1].assets += [$values]' .releaserc.json |sponge .releaserc.json;done
shopt -s globstar && for i in dist/**/*.*; do j='{"path": "'$i'", "label": "'$i'"}';jq --argjson values "$j" '.plugins[-1][-1].assets += [$values]' .releaserc.json |sponge .releaserc.json;done
